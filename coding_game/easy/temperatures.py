import sys, math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

N = int(raw_input()) # the number of temperatures to analyse

if N == 0:
    print 0

TEMPS = [int(i) for i in raw_input().split(" ")] # the N temperatures expressed as integers ranging from -273 to 5526
print >> sys.stderr, TEMPS
DIFF = [abs(x) for x in TEMPS]

OCC = [i for i,x in enumerate(DIFF) if x == min(DIFF)]
print max([TEMPS[i] for i in OCC])


    
# Write an action using print
# To debug: print >> sys.stderr, "Debug messages..."
