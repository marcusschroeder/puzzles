import math._
import scala.util._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 * ---
 * Hint: You can use the debug stream to print initialTX and initialTY, if Thor seems not follow your orders.
 **/
object Player extends App {
    // lightx: the X position of the light of power
    // lighty: the Y position of the light of power
    // initialtx: Thor's starting X position
    // initialty: Thor's starting Y position
    val Array(lightx, lighty, initialtx, initialty) = for(i <- readLine split " ") yield i.toInt
    
    var xDiff = lightx - initialtx
    var yDiff = lighty - initialty
    var thorPosX: Int = initialtx
    var thorPosY: Int = initialty
    
    Console.err.println(lightx)
    Console.err.println(lighty)
    
    var direction = "N"
    
    // game loop
    while(true) {
        val remainingturns = readInt
        
        // Write an action using println
        // To debug: Console.err.println("Debug messages...")
        if (thorPosX != lightx && thorPosY != lighty) {
            
            if (thorPosX < lightx && thorPosY < lighty) {
                direction = "SE"
                thorPosX+=1
                thorPosY+=1
            } else if (thorPosX > lightx && thorPosY < lighty) {
                direction = "SW"
                thorPosX-=1
                thorPosY+=1
            } else if (thorPosX < lightx && thorPosY > lighty) {
                direction = "NE"
                thorPosX+=1
                thorPosY-=1
            } else if (thorPosX > lightx && thorPosY > lighty) {
                direction = "NW"
                thorPosX-=1
                thorPosY-=1
            }
            
            
        } else if (thorPosX != lightx) {
            if(thorPosX < lightx) {
                thorPosX+=1
                direction = "E"
            } else {
                thorPosX-=1
                direction = "W"
            }
        } else if (thorPosY != lighty) {
            if (thorPosY < lighty) {
                thorPosY+=1
                direction = "S"
            } else {
                thorPosY-=1
                direction = "N"
            }
        }
        
         println(direction) // A single line providing the move to be made: N NE E SE S SW W or NW
    }

    
}
