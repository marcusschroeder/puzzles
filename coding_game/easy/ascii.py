import sys, math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

L = int(raw_input())
H = int(raw_input())
T = str(raw_input())

#print >> sys.stderr, L
#print >> sys.stderr, H
#print >> sys.stderr, T
A = "ABCDEFGHIJKLMNOPQRSTUVWXYZ?"

for i in xrange(H):
    ROW = raw_input()
    
    TEXT = ""
    for i,v in enumerate(T):
        index = A.find(v.upper())
        if index == -1:
            index = A.find("?")
        TEXT+= ROW[index*L: index*L+L]
    
    print TEXT
        
    
    

# Write an action using print
# To debug: print >> sys.stderr, "Debug messages..."

