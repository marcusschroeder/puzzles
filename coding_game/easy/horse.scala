import math._
import scala.util._
import scala.collection.mutable.ListBuffer

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {
    val n = readInt
    Console.err.println("Number horses: " + n)
    
    val horses = new ListBuffer[Int]
    for(i <- 0 until n) {
        horses += readInt
    }
    
    val sortedHorseList = horses.sortWith(_ < _)
    var minDiff = 1000000
    var lastHorse = -10000
    
    
    sortedHorseList.foreach(h => {
        val diff = h - lastHorse
        lastHorse = h
        if (diff < minDiff) {
            minDiff = diff
        }
    })
    
    // Write an action using println
    // To debug: Console.err.println("Debug messages...")
    
    println(minDiff)
}
