import math._
import scala.util._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {
    val lon = readLine.replace(",", ".").toDouble
    val lat = readLine.replace(",", ".").toDouble
    val n = readInt
    var min: Double = 1000000
    var minName: String = ""
    for(i <- 0 until n) {
        val defib = readLine.split(";")
        val lonD = defib(4).replace(",", ".").toDouble
        val latD = defib(5).replace(",", ".").toDouble

        val x: Double = (lonD - lon) * cos((lat + latD) / 2)
        val y: Double = (latD - lat)
        val d = sqrt(x * x + y * y) * 6371
        
        if (d < min) {
            min = d
            minName = defib(1)
        }
        
    }
    
    // Write an action using println
    // To debug: Console.err.println("Debug messages...")
    Console.err.println()
    Console.err.println(minName + ": " + min)
    println(minName)
}
