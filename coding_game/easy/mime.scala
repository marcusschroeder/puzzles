import math._
import scala.util._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {
    val n = readInt // Number of elements which make up the association table.
    val q = readInt // Number Q of file names to be analyzed.
    val mimeTypes = scala.collection.mutable.HashMap.empty[String,String]
    for(i <- 0 until n) {
        // ext: file extension
        // mt: MIME type.
        val Array(ext, mt) = readLine split " "
        mimeTypes += (ext.toLowerCase -> mt)
    }

    for(i <- 0 until q) {
        val fname = readLine // One file name per line.
        
        val fArray = fname.split("\\.")
        Console.err.println(fname)
        if (fname.last == '.' || fArray.size < 2 || !mimeTypes.contains(fArray.last.toLowerCase)) {
            println("UNKNOWN")
        } else {
            println(mimeTypes.get(fArray.last.toLowerCase).get)
        }
    }
}
