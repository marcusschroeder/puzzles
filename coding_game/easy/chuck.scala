import math._
import scala.util._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {
    val message = readLine
    var binaryMessage = ""
    message.getBytes.foreach(x => {
            
            val tmp = x.toBinaryString
            
            if (tmp.length < 7) {
                for( i <- 1 to 7 - tmp.length ) {
                    binaryMessage += 0
                }    
            }
            
            binaryMessage += tmp
        
        }
    )
    // Write an action using println
    // To debug: Console.err.println("Debug messages...")
    Console.err.println(message)
    Console.err.println(binaryMessage)
    
    var unaryMessage = ""
    var isOne = false
    var isZero = false
    
    for( i <- 0 to binaryMessage.length - 1) {
        if(binaryMessage(i) == '1') {
            isZero = false        
            if (isOne) {
                unaryMessage += "0"
            } else {
                isOne = true
                unaryMessage += " 0 0"
            }
        } else {
            isOne = false
            if (isZero) {
                unaryMessage += "0"
            } else {
                isZero = true
                unaryMessage += " 00 0"
            }
        }
    }
    println(unaryMessage.substring(1))
}
