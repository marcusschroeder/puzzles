# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

read N

horses=( )
for (( i=0; i<N; i++ )); do
    read Pi
    horses[$i]=$Pi;
done

sortedHorses=( $( printf "%s\n" "${horses[@]}" | sort -n ) )

minDiff=100000
lastHorse=-1000

for i in "${sortedHorses[@]}"
do
    diff=$((i - lastHorse))
    lastHorse=$i
    
    if [ $diff -lt $minDiff ] 
    then
        minDiff=$diff
    fi

done
# Write an action using echo
# To debug: echo "Debug messages..." >&2
echo $minDiff
