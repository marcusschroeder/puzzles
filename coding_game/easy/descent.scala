import math._
import scala.util._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Player extends App {

    // game loop
    while(true) {
        val Array(spacex, spacey) = for(i <- readLine split " ") yield i.toInt
        var mount = List(0,0)
        Console.err.println(mount)
        for(i <- 0 until 8) {
            val mountainh = readInt // represents the height of one mountain, from 9 to 0. Mountain heights are provided from left to right.
            if (mountainh >= mount(1)) {
                mount = List(i, mountainh)
            }
        }
        
        // Write an action using println
        // To debug: Console.err.println("Debug messages...")
        if (mount != (0,0) && spacex == mount(0)) {
            println("FIRE")
        } else {
            println("HOLD")
        }

    }
}
