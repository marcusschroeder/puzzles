import math._
import scala.util._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Player extends App {
    val road = readInt // the length of the road before the gap.
    val gap = readInt // the length of the gap.
    val platform = readInt // the length of the landing platform.
    
    val jumpingIndex = road - 1
    val targetSpeed = gap + 1 
    val landingIndex = road + gap
    var initialSpeed = 0
    var acc_space = 0
    
    // game loop
    while(true) {
        val speed = readInt // the motorbike's speed.
        val coordx = readInt // the position on the road of the motorbike.
        
        
        if (coordx == 0) {
            initialSpeed = speed
            var a = 0
            if (speed < targetSpeed) {
                for( i <- speed to targetSpeed) {
                    a += i
                }
            } else {
                for( i <- targetSpeed to speed) {
                    a += i
                }
            }
            acc_space = a
        }
        // Write an action using println
        // To debug: Console.err.println("Debug messages...")
        Console.err.println(acc_space)
        var ACTION = "WAIT"
    
        if (speed == 0) {
            ACTION = "SPEED"
        } else if (coordx == jumpingIndex) {
            ACTION = "JUMP"
        } else if (coordx >= landingIndex) {
            ACTION = "SLOW"
        } else if (coordx < road - acc_space) {
            ACTION = "WAIT"
        } else if(speed < targetSpeed) {
            ACTION = "SPEED"
        } else if (speed > targetSpeed) {
            ACTION = "SLOW"
        }
        
        println(ACTION)
    }   
}

