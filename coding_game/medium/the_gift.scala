import math._
import scala.util._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {
    val n = readInt
    val c = readInt
    val oods = Array.fill(n)(0)
    for(i <- 0 until n) {
        oods(i) = readInt
    }
    
    if(oods.sum < c) {
        println("IMPOSSIBLE")
    } else {
        var sum = 0
        val sortedOods = oods.sortWith(_ < _)

        for(i <- 0 until n - 1) {
            val s = sortedOods(i)
            val local = (c-sum)/(n-i)
            
            if(s < local) {
                sum += s
                println(s)
            } else {
                sum += local
                println(local)
            }
        }
        
        println(c - sum)
    }
    
}
