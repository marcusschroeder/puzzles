import math._
import scala.util._
import scala.collection.mutable.HashMap
import scala.collection.mutable.SortedSet
import scala.collection.mutable.ListBuffer

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Player extends App {
    // n: the total number of nodes in the level, including the gateways
    // l: the number of links
    // e: the number of exit gateways
    val Array(n, l, e) = for(i <- readLine split " ") yield i.toInt
    var graph = new HashMap[Int, SortedSet[Int]]()
    var gatewayList = ListBuffer[Int]()
    var gatewayEdges = new ListBuffer[Tuple2[Int, Int]]()
    
    // the list of edges that connect the gateway + neighbors to the rest of the graph
    var gatewayConnection = new ListBuffer[Tuple2[Int, Int]]()
    
    for(i <- 0 until n) {
        graph += (i -> SortedSet[Int]())
    }
    
    for(i <- 0 until l) {
        // n1: N1 and N2 defines a link between these nodes
        val Array(n1, n2) = for(i <- readLine split " ") yield i.toInt
        graph(n1) += n2
        graph(n2) += n1
    }
    
    for(i <- 0 until e) {
        val gateway = readInt // the index of a gateway node
        val gatewayNeighbors = graph(gateway)
        gatewayList += gateway 
        
        
        gatewayNeighbors.foreach(n => {
            gatewayEdges += Tuple2(gateway, n)
            
            val secondLevelTargets = graph(n)
            
            secondLevelTargets.foreach(m => {
                if(gateway != m && !gatewayNeighbors.contains(m) && !gatewayConnection.contains(Tuple2(n, m))) {
                    gatewayConnection += Tuple2(m, n)
                }
            })
        })
    }
    
    Console.err.println(graph)
    Console.err.println(gatewayList)
    Console.err.println(gatewayEdges)
    Console.err.println(gatewayConnection)
    // game loop
    while(true) {
        val si = readInt // The index of the node on which the Skynet agent is positioned this turn
        
        // is agent next to gateway?
        var removeEdge: Tuple2[Int, Int] = null
        
        graph(si).foreach(x => {
            val edgeIndex = gatewayEdges.indexOf(Tuple2(x, si))
            if(edgeIndex != -1 && removeEdge == null) {
                removeEdge = Tuple2(x, si)
                println(removeEdge._1 + " " + removeEdge._2)
                gatewayEdges.remove(gatewayEdges.indexOf(removeEdge))
            }
        })
        
        if (removeEdge == null) {
            
            if(!gatewayConnection.isEmpty) {
                removeEdge = gatewayConnection.head
                gatewayConnection.remove(0)    
            } else {
                removeEdge = gatewayEdges.head
                gatewayEdges.remove(0)    
            }
            println(removeEdge._1 + " " + removeEdge._2)
            
        }

    }
}
