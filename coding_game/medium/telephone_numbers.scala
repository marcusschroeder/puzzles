import math._
import scala.util._
import scala.collection.mutable.Set

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {
    val n = readInt
    var prefixes = Set[String]()
    
    for(i <- 0 until n) {
        val number = readLine
        var s = ""
        number.foreach(l => {
            s += l
            if(!prefixes.contains(s)) {
                prefixes += s
            }
        })
    }
    
    println(prefixes.size)
}
