import math._
import scala.util._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {
    val r = readInt
    val l = readInt

    // Write an action using println
    // To debug: Console.err.println("Debug messages...")
    var row = "" + r
    for(i <- 2 to l) {

      var elems = row.split(" ")
      var elem = elems(0)
      var count = 0
      var nextRow = ""
      elems.zipWithIndex.foreach(t => {
        if(elem == t._1) {
          count += 1
        } else {
          nextRow += count + " " + elem + " "
          elem = t._1
          count = 1
        }
      })

      nextRow += count + " " + elem

      row = nextRow
      Console.err.println("Nextline: " + nextRow)
    }

    println(row)
}
