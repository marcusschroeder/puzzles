import math._
import scala.util._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {
    
    def computeDistance(base: Int): BigInt = {
        (BigInt(0) /: houses) { (z: BigInt, h) => z + abs(h - base)}
    }
    
    val n = readInt
    val houses = Array.fill[Int](n)(0)
    var minX, minY = 1000000000
    var maxX, maxY = -1000000000
    for(i <- 0 until n) {
        val Array(x, y) = for(i <- readLine split " ") yield i.toInt
        houses(i) = y
        
        if(x < minX) minX = x
        if(x > maxX) maxX = x
        if(y < minY) minY = y
        if(y > maxY) maxY = y
        
    }
    
    val range = {
        if(maxY - minY < 10000) {
            (minY to maxY).toList
        } else {
            if(n < 1000) {
                houses.toList   
            } else {
                val avg = ((houses.sum/houses.size.toFloat)).toInt
                (avg - 10 to avg + 10).toList    
            }
        }
    }
    val distance = range.map(computeDistance(_)).min
    println((distance + abs(maxX - minX)))
}
