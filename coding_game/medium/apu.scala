import math._
import scala.util._
import scala.collection.mutable.ListBuffer

/**
 * Don't let the machines win. You are humanity's last hope...
 **/
object Player extends App {
    val width = readInt // the number of cells on the X axis
    val height = readInt // the number of cells on the Y axis
    val nodes = new ListBuffer[Tuple2[Int, Int]]()
    
    for(row <- 0 until height) {
        val cells = readLine // width characters, each either 0 or .
        for(col <- 0 until cells.size) {
            if(cells(col) == '0') {
                nodes += Tuple2(col, row)
            }
        }
    }
    
    Console.err.println(nodes)
    nodes.foreach(n => {
        var rN: Tuple2[Int, Int] = null
        var bN: Tuple2[Int, Int] = null
        
        var x = n._1 + 1
        while(x <= width && rN == null) {
            val t = Tuple2(x, n._2)
            if(nodes.contains(t)) {
                rN = t
            }
            x += 1
        }
        
        var y = n._2 + 1
        while(y <= height && bN == null) {
            val t = Tuple2(n._1, y)
            if(nodes.contains(t)) {
                bN = t
            }
            y += 1
        }
        
        if(rN == null) rN = Tuple2(-1, -1)
        if(bN == null) bN = Tuple2(-1, -1)
        
        println(n._1 + " " + n._2 + " " + rN._1 + " " + rN._2 + " " + bN._1 + " " + bN._2)
    }) 
    
    
    // Write an action using println
    // To debug: Console.err.println("Debug messages...")
    
    println("0 0 1 0 0 1") // Three coordinates: a node, its right neighbor, its bottom neighbor
}
