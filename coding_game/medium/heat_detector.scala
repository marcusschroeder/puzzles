import math._
import scala.util._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

object Player extends App {
    // w: width of the building.
    // h: height of the building.
    val Array(w, h) = for(i <- readLine split " ") yield i.toInt
    val n = readInt // maximum number of turns before game over.
    var Array(x0, y0) = for(i <- readLine split " ") yield i.toInt
    var minX = 0
    var minY = 0
    var maxX = w
    var maxY = h
    // game loop
    Console.err.println(maxX + " " + maxY + " - ")
    while(true) {
        val bombDir = readLine // the direction of the bombs from batman's current location (U, UR, R, DR, D, DL, L or UL)
        Console.err.println(bombDir)
        
        if (bombDir.contains("U")) jumpUp
        if (bombDir.contains("R")) jumpRight
        if (bombDir.contains("D")) jumpDown
        if (bombDir.contains("L")) jumpLeft
        
        println(x0 + " " + y0)
        
    }
    
    def jumpRight(): Unit = {
        minX = x0
        x0 = x0 + (maxX - x0)/2
    }
    
    def jumpLeft(): Unit = {
        Console.err.println(x0 + " " + minX + " " + (x0 - minX)/2)
        maxX = x0
        x0 = x0 - ceil((x0 - minX)/2.0).toInt
    }
    
    def jumpUp(): Unit = {
        maxY = y0
        y0 = y0 - ceil((y0 - minY)/2.0).toInt
    }
    
    def jumpDown(): Unit = {
        minY = y0
        y0 = y0 + (maxY - y0)/2
    }
}
