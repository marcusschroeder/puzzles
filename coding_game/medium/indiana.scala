import math._
import scala.util._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Player extends App {
    val t = "TOP"
    val b = "BOTTOM"
    val r = "RIGHT"
    val l = "LEFT"
    
    val tiles = List[Map[String, String]](
        Map(), //type 0
        Map(t -> b, l -> b, r -> b), //type 1
        Map(l -> r, r -> l), //type 2
        Map(t -> b), //type 3
        Map(t -> l, r -> b), //type 4
        Map(t -> r, l -> b), //type 5
        Map(l -> r, r -> l), //type 6
        Map(t -> b, r -> b), //type 7
        Map(l -> b, r -> b), //type 8
        Map(l -> b, t -> b), //type 9
        Map(t -> l), //type 10
        Map(t -> r), //type 11
        Map(r -> b), //type 12
        Map(l -> b) //type 13
    )
    
    // w: number of columns.
    // h: number of rows.
    val Array(w, h) = for(i <- readLine split " ") yield i.toInt
    val lines = Array.fill(h)("")
    for(i <- 0 until h) {
        lines(i) = readLine // represents a line in the grid and contains W integers. Each integer represents one room of a given type.
    }
    val ex = readInt // the coordinate along the X axis of the exit (not useful for this first mission, but must be read).

    // game loop
    while(true) {
        val Array(_xi, _yi, pos) = readLine split " "
        val xi = _xi.toInt
        val yi = _yi.toInt
        
        val currentTileType = lines(yi).split(" ")(xi).toInt
        val currentTile = tiles(currentTileType)
        
        val exit = currentTile(pos)
        
        val (newX, newY) = exit match {
            case this.l => (xi-1, yi)
            case this.r => (xi+1, yi)
            case this.b => (xi, yi + 1)
        }
        
        println(newX + " " + newY)
        
    }
}
