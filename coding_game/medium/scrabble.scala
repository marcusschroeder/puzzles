import math._
import scala.util._
import scala.collection.mutable.Set
import scala.collection.mutable.HashMap

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {
    val n = readInt

    val scoring = Map(
        'e' -> 1,
        'a' -> 1,
        'i' -> 1,
        'o' -> 1,
        'n' -> 1,
        'r' -> 1,
        't' -> 1,
        'l' -> 1,
        's' -> 1,
        'u' -> 1,
        'd' -> 2,
        'g' -> 2,
        'b' -> 3,
        'c' -> 3,
        'm' -> 3,
        'p' -> 3,
        'f' -> 4,
        'h' -> 4,
        'v' -> 4,
        'w' -> 4,
        'y' -> 4,
        'k' -> 5,
        'j' -> 8,
        'x' -> 8,
        'q' -> 10,
        'z' -> 10
    )

    val words = HashMap[String, String]()

    var t1 = System.nanoTime
    for(i <- 0 until n) {
        val word = readLine


        val sortedWord = getStringSorted(word)
        if(!words.contains(sortedWord)) {
            words += (sortedWord -> word)
        }

    }

    var t2 = System.nanoTime
    var t3 = t2 - t1
    Console.err.println("Dictionary:" + t3/1000000 + " ms")

    val letters = getStringSorted(readLine)
    var maxScore = 0
    var maxWord = ""

    t1 = System.nanoTime()
    (1 until math.pow(2, letters.length).toInt).foreach(k => {
        val s = k.toBinaryString.zipWithIndex.foldLeft("")((s, e) => {
            if(e._1 == '1') {
                s + letters(e._2)
            } else {
                s
            }
        })

        val score = getWordValue(s)
        if(score > maxScore && words.contains(s)) {
            maxScore = score
            maxWord = words(s)
        }
    })

    t2 = System.nanoTime()

    var t4 = t2 - t1
    Console.err.println("Permutations:" + t4/1000000 + " ms")
    Console.err.println("Total:" + (t4 + t3)/1000000 + " ms")
    println(maxWord)


    def getStringSorted(w: String): String = {
        w.toCharArray.sortWith((c1, c2) => c1.toInt < c2.toInt).foldLeft("")((s, i) => s + i)
    }

    def getWordValue(w: String): Int = {
        w.foldLeft(0)((s,i) => s + scoring(i))
    }
}
