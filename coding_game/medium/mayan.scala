import math._
import scala.util._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {
  val Array(l, h) = for(i <- readLine split " ") yield i.toInt
  var numerals = Array.fill(20)("")
  for(i <- 0 until h) {
    readLine.grouped(l).zipWithIndex.foreach(t => numerals(t._2) += t._1)
  }

  Console.err.println("S1")
  val s1 = readInt
  var v1: Long = 0
  var n1 = ""
  for(i <- 0 until s1) {

    n1 += readLine
    if(i % l == l - 1) {
      v1 += numerals.indexOf(n1) * math.pow(20, (s1 - i + 1)/l).toInt
      Console.err.println(numerals.indexOf(n1) + " * 20 ^ " + (s1-i+1)/l)
      n1 = ""
    }
  }
  Console.err.println("== " + v1)


  Console.err.println("S2")
  val s2 = readInt
  var v2: Long = 0
  var n2 = ""
  for(i <- 0 until s2) {

    n2 += readLine
    if(i % l == l - 1) {
      v2 += numerals.indexOf(n2) * math.pow(20, (s2 - i + 1)/l).toInt
      Console.err.println(numerals.indexOf(n2) + " * 20 ^ " + (s2-i+1)/l)
      n2 = ""
    }
  }

  Console.err.println("== " + v2)

  val operation = readLine

  Console.err.println("Operation: " + operation)
  var value: Long = 0

  if (operation == "+") value = v1 + v2
  else if (operation == "*") value = v1 * v2
  else if (operation == "/") value = v1 / v2
  else if (operation == "-") value = v1 - v2


  //back to mayan
  if(value == 0) {
    numerals(0).grouped(l).foreach(println(_))
  } else {
    var restValue: Long = value
    for(i <- 0 to 20) {

      val p = math.pow(20, 20 - i)
      Console.err.println(restValue + "  / " + p)
      if(restValue >= p) {
        val base = math.floor(restValue/p)
        Console.err.println("FOUND: " + restValue +" / " + p + " == " +base + " * 20 ^ " + (20 - i))
        numerals(base.toInt).grouped(l).foreach(println(_))

        restValue -= (base * p).toLong
      } else if(restValue < value) {
        numerals(0).grouped(l).foreach(println(_))
      }
    }
  }



}