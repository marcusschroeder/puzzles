import math._
import scala.util._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Solution extends App {
    val n = readInt
    val vs = readLine.split(" ")
    
    val start = vs(0).toInt
    var high = start
    var lastValue = start
    var low = start
    var maxLoss = 0
    
    vs.foreach(x => {
        val v = x.toInt
        if(v < low) {
            
            low = v
            if(low - high < maxLoss) {
                maxLoss = low - high
            }
        }

        if(v > high) {
            high = v
            low = v
        }
        

        lastValue = v
    })

    
    
    // Write an action using println
    // To debug: Console.err.println("Debug messages...")
    
    println(maxLoss)
}
