import math._
import scala.util._

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
object Player extends App {
    // nbfloors: number of floors
    // width: width of the area
    // nbrounds: maximum number of rounds
    // exitfloor: floor on which the exit is found
    // exitpos: position of the exit on its floor
    // nbtotalclones: number of generated clones
    // nbadditionalelevators: ignore (always zero)
    // nbelevators: number of elevators
    val Array(nbfloors, width, nbrounds, exitfloor, exitpos, nbtotalclones, nbadditionalelevators, nbelevators) = for(i <- readLine split " ") yield i.toInt
    
    var elevators = Array.fill(nbfloors){exitpos}
    for(i <- 0 until nbelevators) {
        // elevatorfloor: floor on which this elevator is found
        // elevatorpos: position of the elevator on its floor
        val Array(elevatorfloor, elevatorpos) = readLine split " "
        elevators(elevatorfloor.toInt) = elevatorpos.toInt
    }
    

    // game loop
    while(true) {
        // clonefloor: floor of the leading clone
        // clonepos: position of the leading clone on its floor
        // direction: direction of the leading clone: LEFT or RIGHT
        val Array(_clonefloor, _clonepos, direction) = readLine split " "
        val clonefloor = _clonefloor.toInt
        val clonepos = _clonepos.toInt

        if(clonepos == -1) {
            println("WAIT")
        } else {
            val targetpos = elevators(clonefloor)
            if ((clonepos < targetpos && direction == "LEFT") || (clonepos > targetpos && direction == "RIGHT")) {
                println("BLOCK")
            } else {
                println("WAIT")
            }
        } 
    }
}
