package com.marcusschroeder.algorithms.sorting

import scala.util.Random

object SortingTest {
  private def assertionsTest(callback: (List[Int]) => List[Int]): Unit = {
    val test1 = List(5, 4, 3, 2, 1)
    val expected1 = List(1, 2, 3, 4, 5)
    val test2 = List(10, -10, 0, -5)
    val expected2 = List(-10, -5, 0, 10)
    val test3 = List(3, 4, 2, 1, 7, 5, 8, 9, 0, 6)
    val expected3 = List(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
    val message: String = "Sorting does not match"

    assert(callback(test1) == expected1, message)
    assert(callback(test2) == expected2, message)
    assert(callback(test3) == expected3, message)
  }

  private def measurement[T](n: Int, callback: (List[Int]) => List[Int]): Unit = {
    val longList = List.fill(n)(Random.nextInt)
    val time1 = System.nanoTime()
    callback(longList)
    val time2 = System.nanoTime()
    println(f"Elements: $n%s \t Time: ${(time2 - time1) / (1000000.0)}ms")
  }

  private def performanceTest[T](callback: (List[Int]) => List[Int]): Unit = {
    measurement(10, callback)
    measurement(100, callback)
    measurement(1000, callback)
  }

  def verify(callback: (List[Int]) => List[Int]): Unit = {
    println("Testing: " + callback.toString())
    assertionsTest(callback)
    performanceTest(callback)
  }
}
