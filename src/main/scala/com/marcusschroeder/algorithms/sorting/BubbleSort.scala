package com.marcusschroeder.algorithms.sorting

import scala.collection.mutable.ListBuffer

/**
  * Bubble Sort
  * Worst Case: O(n2)
  */
object BubbleSort {

  /**
    * Classical imperative implementation of Bubble Sort
    * @param list
    * @tparam T
    * @return
    */
  def sort[T <% Ordered[T]](list: List[T]): List[T] = {

    val sortedList = ListBuffer(list: _*)

    for (n <- sortedList.size until 1 by -1) {
      for(i <- 0 until n - 1) {
        if(sortedList(i) >  sortedList(i + 1)) {

          val tmp = sortedList(i)
          sortedList(i) = sortedList(i + 1)
          sortedList(i + 1) = tmp
        }
      }
    }

    return sortedList.toList
  }

  /**
    * Optimized imperative Bubble Sort implementation
    * @param list
    * @tparam T
    * @return
    */
  def optimizedSort[T <% Ordered[T]](list: List[T]): List[T] = {

    val sortedList = ListBuffer(list: _*)
    var n = sortedList.size
    do {
      var new_n = 1
      for(i <- 0 until n - 1) {
        if(sortedList(i) >  sortedList(i + 1)) {

          val tmp = sortedList(i)
          sortedList(i) = sortedList(i + 1)
          sortedList(i + 1) = tmp

          new_n +=1
        }
      }
      n = new_n

    } while(n > 1)

    return sortedList.toList
  }

  /**
    * Functional Bubble Sort implementation
    * @param inputList
    * @tparam T
    * @return
    */
  def functionalSort[T <% Ordered[T]](inputList: List[T]): List[T] = {


    def sort(source: List[T], result: List[T]) = {
      if (source.isEmpty) result
      else bubble(source, Nil, result)
    }

    def bubble(source: List[T], tempList: List[T], result: List[T]): List[T] = {
      source match {
        case h1 :: h2 :: t => {
          if (h1 > h2) bubble(h1 :: t, h2 :: tempList, result)
          else bubble(h2 :: t, h1 :: tempList, result)
        }
        case h1 :: t => {
          sort(tempList, h1 :: result)
        }
      }
    }

    sort(inputList, Nil)
  }
}
