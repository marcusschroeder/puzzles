package com.marcusschroeder.algorithms.sorting

/**
  * Merge Sort
  * Worst Case O(n * log n)
  */
object MergeSort {

  /**
    * Classical imperative implementation of Merge Sort
    * @param A
    * @tparam T
    * @return
    */
  def sort[T <% Ordered[T]](A: Array[T]): Array[T] = {

    for (i <- 1 until A.size) {
      val x = A(i)
      var j = i

      while (j > 0 && A(j - 1) > x) {
        A(j) = A(j - 1)
        j -= 1
      }

      A(j) = x
    }

    return A
  }

  /**
    * Functional implementation of Merge Sort
    * @param inputList
    * @tparam T
    * @return
    */
  def functionalSort[T <% Ordered[T]](inputList: List[T]): List[T] = {

    def sort(list: List[T]): List[T] = {
      list.length match {
        case x if x <= 1 => list
        case _ => {
          val (l, r) = list.splitAt(list.length / 2)
          val leftList = sort(l)
          val rightList = sort(r)
          merge(leftList, rightList, Nil)
        }
      }
    }

    def merge(leftList: List[T], rightList: List[T], newList: List[T]): List[T] = {
      if (!leftList.isEmpty && !rightList.isEmpty) {
        if (leftList.head <= rightList.head) {
          merge(leftList.tail, rightList, newList :+ leftList.head)
        } else {
          merge(leftList, rightList.tail, newList :+ rightList.head)
        }
      } else if (!leftList.isEmpty) {
        merge(leftList.tail, rightList, newList :+ leftList.head)
      } else if (!rightList.isEmpty) {
        merge(leftList, rightList.tail, newList :+ rightList.head)
      } else {
        newList
      }
    }

    sort(inputList)
  }
}
