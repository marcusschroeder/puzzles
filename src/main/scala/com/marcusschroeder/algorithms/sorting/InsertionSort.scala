package com.marcusschroeder.algorithms.sorting

import scala.collection.mutable.ListBuffer
import scala.util.Random

/**
  * Insertion Sort
  * Worst Case: O(n2)
  */
object InsertionSort {

  /**
    * Classical imperative implementation of Insertion Sort
    * @param list
    * @tparam T
    * @return
    */
  def sort[T <% Ordered[T]](list: List[T]): List[T] = {

    val sortedList = ListBuffer(list: _*)

    for (i <- 1 until list.size) {
      val x = list(i)
      var j = i

      while (j > 0 && sortedList(j - 1) > x) {
        sortedList(j) = sortedList(j - 1)
        j -= 1
      }

      sortedList(j) = x
    }

    return sortedList.toList
  }

  /**
    * Functionla Insertion Sort implementation
    * @param inputList
    * @tparam T
    * @return
    */
  def functionalSort[T <% Ordered[T]](inputList: List[T]): List[T] = {

    def sort(begin: List[T], rest: List[T]) = {
      if (rest.isEmpty) begin
      else insert(begin, rest)
    }

    def insert(begin: List[T], rest: List[T]): List[T] = {
      rest match {
        case element :: newRest => {
          val i = begin.indexWhere(element < _ )
          /*
           If condition is not satisfied, i is -1 but then prev will be an empty list.
          */
          val (prev, succ) = begin.splitAt(i)
          insert((prev :+ element) ++ succ, newRest)
        }
        case _ => {
          sort(begin, Nil)
        }
      }
    }

    sort(Nil, inputList)
  }
}
