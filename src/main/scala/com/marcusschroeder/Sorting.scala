import com.marcusschroeder.algorithms.sorting.{BubbleSort, SortingTest}

SortingTest.verify(BubbleSort.sort[Int])
SortingTest.verify(BubbleSort.optimizedSort[Int])
SortingTest.verify(BubbleSort.functionalSort[Int])


