'''
https://projecteuler.net/problem=38
'''
import time


t1 = time.time()

# upper limit must be 987654321, lower limit is 918273645
for i in range(9876, 9182, -1):
    if reduce(lambda x, y: x + y, sorted(str(i) + str(i * 2))) == "123456789":
        print i, i * 2
        break

t2 = time.time()
print round((t2 - t1) * 1000, 2), "ms"