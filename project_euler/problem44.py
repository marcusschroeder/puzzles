'''
https://projecteuler.net/problem=44
'''
import time


t1 = time.time()
l = []
P_n = [n * (3 * n - 1) / 2 for n in range(1, 1000)]

for i, v_i in enumerate(P_n):
    for j, v_j in enumerate(P_n):

        if i < j:
            s = v_i + v_j
            d = v_j - v_i
            if s in P_n:
                # print "SUM: ", i, ":", v_i, "+", j, ":", v_j, "==", P_n.index(s), ":", s
                if P_n.index(s) not in l:
                    l.append(P_n.index(s))
            # if d in P_n:
            #     print "DIFF: ", i, ":", v_i, "-", j, ":", v_j, "==", P_n.index(d), ":", d

print len(l)
print sorted(l)

t2 = time.time()
print (t2 - t1) * 1000, "ms"
