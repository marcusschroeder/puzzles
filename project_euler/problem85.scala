/**
  * By counting carefully it can be seen that a rectangular grid measuring 3 by 2 contains eighteen rectangles:
  * Although there exists no rectangular grid that contains exactly two million rectangles, find the area of the grid with the nearest solution.
  */


/* formular for calculating number of rectangles

n: size of row
m: size of column

f(x) = (x + 1) * x / 2 #number of rectangles per row/column

g(n, m) = f(n) * f(m) #number of rectangles  per grid n x m

*/

def f(x: Int): Int = {
  (x + 1) * x / 2
}

def g(n:Int , m:Int): Int = {
  f(n) * f(m)
}


assert(1 == f(1))
assert(3 == f(2))
assert(6 == f(3))
assert(10 == f(4))

assert(1 == g(1, 1))
assert(3 == g(1, 2))
assert(9 == g(2, 2))


val limit = 2000000
val boundary = 100

var minimum = limit
var grid = (0, 0)

for (i: Int <- 0 to boundary) {
  var j: Int = i

  for(j: Int <- 0 to boundary) {
    val r = Math.abs(g(i, j) - limit)
    if (r < minimum) {
      minimum = r
      grid = (i, j)
    }
  }
}

val t1 = System.nanoTime()
println("RESULT Area: " + grid._1 * grid._2 + " (n, m): " + grid + " rectangles:" + g(grid._1, grid._2))
val t2 = System.nanoTime()
println("TIME: " + ((t2 - t1) / 1000000000.0) + "ms")
