'''
https://projecteuler.net/problem=58
'''
import tools

noOfPrimes = 3.0
sl = 2
c = 9

while noOfPrimes / (2 * sl + 1) > 0.1:
    print sl, (sl + 1) * (sl + 1), noOfPrimes, noOfPrimes / (2 * sl + 1)
    sl += 2
    for i in range(3):
        c += sl
        if tools.is_prime(c):
            noOfPrimes += 1.0
    c += sl


print sl+1