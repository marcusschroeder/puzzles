'''
https://projecteuler.net/problem=45

apparently the computation of triangular numbers can be skipped since every hexagonal number is a triangle number

solve H_n = P_n by finding the the matching n for H_n
'''
import math
import time

t1 = time.time()
match = False
n = 166
while not match:
    P_n = n * (3 * n - 1) / 2

    o = math.floor((math.sqrt(12 * n * n - 4 * n + 1) + 1) / 4)
    H_n = o * (2 * o - 1)

    if P_n == H_n:
        print "Number: ", P_n
        match = True

    n += 1
t2 = time.time()
print (t2 - t1) * 1000, "ms"