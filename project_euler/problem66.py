'''
https://projecteuler.net/problem=66
'''
import time
import math
import tools


def find_solution(d):
    y = 1
    d = float(d)
    while True:
        x = math.sqrt(y * y * d + 1)
        if x % 1 == 0:
            break
        y += 1

    return x


t1 = time.time()
upper_limit = 30
d_max = 0
d_max_value = 0

primes = tools.primes(upper_limit)

for i in range(1, upper_limit):
    if not math.sqrt(i) % 1 == 0:
        x = find_solution(i)
        if x > d_max_value:
            d_max_value = x
            d_max = i

# print "d:", d_max, "x:", d_max_value

t2 = time.time()
print round(t2 - t1, 4), "s"
