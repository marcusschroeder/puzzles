def numberDigits(n: Int): Double = {
  9 * n * Math.pow(10, n - 1)
}

/**
  * Returns the complete number at the given index
  * @param idx
  * @param n
  * @return
  */
def getNumberAtIndex(idx: Int, n: Int): Double = {
  Math.pow(10, n) + idx / (n + 1)
}

def getStartingIndex(idx: Int): (Int, Int) = {
  var i = 0.0
  var n = 1
  while(i + numberDigits(n) < idx) {
    i += numberDigits(n)
    n += 1
  }
  ( 1 + i.toInt , n - 1)
}

def getDigit(idx: Int): Int = {
  val (i, n) = getStartingIndex(idx)
  val start = idx - i
  getNumberAtIndex(start, n).toString.charAt(start % (n + 1)).toString.toInt
}

assert(1 == getDigit(1))
assert(2 == getDigit(2))
assert(3 == getDigit(3))
assert(4 == getDigit(4))
assert(1 == getDigit(10))
assert(0 == getDigit(11))
assert(1 == getDigit(12))
assert(1 == getDigit(13))
assert(1 == getDigit(14))
assert(2 == getDigit(15))
assert(1 == getDigit(28))
assert(9 == getDigit(29))

val t1 = System.nanoTime();
var p = 1
for (i <- 0 to 6) {
  val idx = Math.pow(10, i).toInt;
  p *= getDigit(idx)
}
val t2 = System.nanoTime();

println("RESULT: " + p)
println("Computed in:" + (t2-t1)/(Math.pow(10, 6)) + "ms")
