'''
https://projecteuler.net/problem=27
'''
import tools
import time


def count_consecutive_primes(a, b):
    n = 0
    while True:
        x = n * n + a * n + b
        if tools.is_prime(x):
            n += 1
        else:
            break
    return n


t1 = time.time()
max_n = -1
max_a = -1
max_b = -1
for a in range(-1000, 1001):
    for b in range(-1000, 1001):
        n = count_consecutive_primes(a, b)
        if n > max_n:
            max_n = n
            max_a = a
            max_b = b

print max_n, max_a, max_b
t2 = time.time()
print round((t2 - t1) * 1000, 2), "ms"