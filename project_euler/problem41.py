'''
https://projecteuler.net/problem=41

We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital and is also prime.

What is the largest n-digit pandigital prime that exists?
'''
import itertools
import tools

permutations = list(itertools.permutations(['7', '6', '5', '4', '3', '2', '1'], 7))
permutations = [int(''.join(i)) for i in permutations]

for i in permutations:
    if tools.is_prime(i):
        print i
        break