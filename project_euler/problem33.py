'''
https://projecteuler.net/problem=33

The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s.

We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator.

If the product of these four fractions is given in its lowest common terms, find the value of the denominator.

Info:
Looking for pairs that fulfil the condition:

ab/bc = a/c

Where ab is a digit made of two digits. 65 (a = 6, b=5)

'''

nominator = 1
denominator = 1
count = 1

for a in range(1, 10):
    for b in range(a, 10):
        for c in range(1, 10):
            count += 1
            if a != b and a != c:
                x = int(str(a) + str(b))
                y = int(str(b) + str(c))

                if float(a) / c == float(x) / y:
                    nominator *= x
                    denominator *= y
                    print a, b, c


print float(nominator)/denominator