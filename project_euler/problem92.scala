
// Brute force
val t1 = System.nanoTime()
val n  = 10000001

var count89 = 0;
var count1 = 0;
var i: Int = 1
val bucketLimit = 567 // 9.999.999 -> 81 * 7 = 567

var list: List[Int] = List(0)

while(i <= bucketLimit) {

  var result = i
  while(result != 89 && result != 1) {
    val s = result.toString.split("").map(x => x.toInt)
    result = s.foldLeft(0)({(r, x: Int) => r + x * x })
  }

  if (result == 89) {
    count89 +=1
  } else {
    count1 += 1
  }

  list = result :: list

  i += 1
}

list = list.reverse

while(i <= n) {

  val s = i.toString.split("").map(x => x.toInt)
  val result = list(s.foldLeft(0)({(r, x: Int) => r + x * x }))

  if (result == 89) {
    count89 +=1
  } else {
    count1 += 1
  }

  i += 1
}

val t2 = System.nanoTime()
println("COUNT 89: " + count89) //Correct: 8581146
println("COUNT 1: " + count1)
println("TIME: " + ((t2 - t1) / 1000000000.0) + "ms")
