'''
https://projecteuler.net/problem=32
'''
import time
import itertools

t1 = time.time()
t = itertools.permutations(range(1, 10), 9)

total = set()
for i in t:
    s = ''.join(str(e) for e in i)
    for j in range(1, 5):
        for k in range(1, 5 - j + 1):
            a = int(s[:j])
            b = int(s[j: j + k])
            c = int(s[j + k:])

            if a * b == c:
                print s, a, b, c, a * b
                total.add(c)

print total
print "sum:", sum(total)
t2 = time.time()
print round(t2 - t1, 2), "s"
