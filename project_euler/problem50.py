'''
https://projecteuler.net/problem=50
'''
import tools
import time


def findPrime(n):
    p = tools.primes(n/4) #quarter search space because adding primes will grow quickly

    l = 0
    prime = -1
    s = []
    for i in range(len(p) - 1):
        for j in range(i+1, len(p) - 1):
            a = sum(p[i:j])

            if(a >= n):
                break

            if(l < j - i and tools.is_prime(a)):
                prime = a
                l = j - 1
                s = p[i:j]

    return prime, l, s

print findPrime(100)[0], "should be 41"
print findPrime(1000)[0], "should be 953"
t1 = time.time()
print "result:" , findPrime(1000000)[0]
t2 = time.time()
print round((t2 - t1) * 1000, 2), "ms"
