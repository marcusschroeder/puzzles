'''
https://projecteuler.net/problem=61
'''
import itertools
import time


def overlaps_t(t, seq):
    new_t = []
    for i in t:
        for j in seq:
            suffix = str(i[-1])[2:]
            prefix = str(j)[:2]
            if prefix == suffix:
                i.append(j)
                new_t.append(i)

    return new_t


def overlaps_circle(t):
    new_t = []
    for i in t:
        suffix = str(i[-1])[2:]
        prefix = str(i[0])[:2]
        if suffix == prefix:
            new_t.append(i)

    return new_t


t1 = time.time()
triangle = [1]
square = [1]
pentagonal = [1]
hexagonal = [1]
heptagonal = [1]
octagonal = [1]

n = 1
done = False
while not done:
    done = True
    n += 1
    if triangle[-1] < 10000:
        t = n * (n + 1) / 2
        if t > 999:
            triangle.append(t)
        done = False

    if square[-1] < 10000:
        t = n * n
        if t > 999:
            square.append(t)
        done = False

    if pentagonal[-1] < 10000:
        t = n * (3 * n - 1) / 2
        if t > 999:
            pentagonal.append(t)
        done = False

    if hexagonal[-1] < 10000:
        t = n * (2 * n - 1)
        if t > 999:
            hexagonal.append(t)
        done = False

    if heptagonal[-1] < 10000:
        t = n * (5 * n - 3) / 2
        if t > 999:
            heptagonal.append(t)
        done = False

    if octagonal[-1] < 10000:
        t = n * (3 * n - 2)
        if t > 999:
            octagonal.append(t)
        done = False

l = [triangle, square, pentagonal, hexagonal, heptagonal, octagonal]

p = itertools.permutations([0, 1, 2, 3, 4, 5], 6)

for i in list(p):

    a = [[k] for k in l[i[0]]]
    t = overlaps_t(a, l[i[1]])

    for j in range(2, 6):
        t = overlaps_t(t, l[i[j]])

    t = overlaps_circle(t)
    if len(t) > 0 and len(t[0]) < 7:
        print len(t), t, i, sum(t[0])
        break

t2 = time.time()
print round((t2 - t1) * 1000, 1), "ms"