'''
https://projecteuler.net/problem=49

The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: (i) each of the three terms are prime, and, (ii) each of the 4-digit numbers are permutations of one another.

There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but there is one other 4-digit increasing sequence.

What 12-digit number do you form by concatenating the three terms in this sequence?
'''

import tools

buckets = {}

for i in range(1000, 10000):
    if tools.is_prime(i):
        bucket_key = ''.join(sorted(str(i)))

        if buckets.has_key(bucket_key):
            buckets[bucket_key].append(i)
        else:
            buckets[bucket_key] = [i]

for key, value in buckets.iteritems():
    diffs = {}

    if len(value) > 2:
        for i, v in enumerate(value):
            for j in value[i + 1:]:
                x = j - v

                if diffs.has_key(str(x)):
                    diffs[str(x)].append((v, j))
                else:
                    diffs[str(x)] = [(v, j)]


    for k, v in diffs.iteritems():
        if len(v) > 1 and v[0][1] - v[0][0] == v[1][1] - v[0][1]:
            print k, v