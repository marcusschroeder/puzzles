'''
https://projecteuler.net/problem=39

If p is the perimeter of a right angle triangle with integral length sides, {a,b,c}, there are exactly three solutions for p = 120.

{20,48,52}, {24,45,51}, {30,40,50}

For which value of p ≤ 1000, is the number of solutions maximised?
'''
max_number = 0
max_index = 0
for p in range(1000,1001):
    number = 0
    print p
    for a in range(p, p/3, -1):
        for b in range(p-a, 1, -1):
            c = p - a - b
            print a, b, c, a + b + c
            if a * a == b * b + c * c and a > b and a > c and b > c:
                number += 1

    if number > max_number:
        max_number = number
        max_index = p

print max_index, max_number