'''
The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.

Find the sum of the only eleven primes that are both truncatable from left to right and right to left.

NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
'''
import tools

truncatable = []

n = 11
while len(truncatable) < 11:
    if tools.is_prime(n):
        is_truncatable = True
        x = str(n)
        right = [int(x[:i + 1]) for i, v in enumerate(x)]

        for i in right:
            if not tools.is_prime(i):
                is_truncatable = False
                break

        if is_truncatable:
            left = [int(x[i:]) for i, v in enumerate(x)]
            for i in left:
                if not tools.is_prime(i):
                    is_truncatable = False
                    break

        if is_truncatable:
            truncatable.append(n)
            print truncatable

    n += 2

print truncatable, sum(truncatable)

