import scala.io.Source._


def findMin(file: String): Int = {
  val lines = fromFile(file).getLines
  val matrix = lines.map({ l: String => l.split(",").map(_.toInt) }).toArray

  val dim = matrix.size

  val sumMatrix = Array.tabulate(dim, dim)((x, y) => 0)

  sumMatrix(0)(0) = matrix(0)(0)

  (1 until dim).foreach({x: Int => sumMatrix(x)(0) = sumMatrix(x - 1)(0) + matrix(x)(0)})
  (1 until dim).foreach({x: Int => sumMatrix(0)(x) = sumMatrix(0)(x - 1) + matrix(0)(x)})

  for (i <- 1 until dim) {
    for (j <- 1 until dim) {
      val idx = Math.max(i - 1, 0)
      val jdx = Math.max(j - 1, 0)
      sumMatrix(i)(j) = Math.min(sumMatrix(idx)(j), sumMatrix(i)(jdx)) + matrix(i)(j)
    }
  }
  return sumMatrix(dim - 1)(dim -1)
}

assert(2427 == findMin("res/problem81/5x5matrix.txt"))

val t1 = System.nanoTime()
println("RESULT: " + findMin("res/problem81/80x80matrix.txt"))
val t2 = System.nanoTime()
println("TIME: " + (t2 - t1) / 1000000000.0  + "ms")
