'''
https://projecteuler.net/problem=35

The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.

There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

How many circular primes are there below one million?
'''
import tools

count = 0;
prime_counter = 0;
for i in range(1000001):
    if tools.is_prime(i):
        prime_counter+=1

        str_i = str(i)
        is_circular = True
        for j in range(len(str_i) -1):
            str_i = str_i[1:] + str_i[0]
            if not tools.is_prime(int(str_i)):
                is_circular = False
                break;

        if is_circular:
            count += 1


print prime_counter, count