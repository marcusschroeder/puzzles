'''
https://projecteuler.net/problem=36
'''
import time

t1 = time.time()

# only taking uneven numbers into account, since only they can be a palindrome
print sum([i for i in range(1, 1000001, 2) if str(i) == str(i)[::-1] and bin(i)[2:] == bin(i)[:1:-1]])
t2 = time.time()

print round(t2 - t1, 2), "s"