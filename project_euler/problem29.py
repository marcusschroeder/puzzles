'''
https://projecteuler.net/problem=29
'''
import math
import time

t1 = time.time()
c = set()

for a in range(2, 101):
    for b in range(2, 101):
        c.add(math.pow(a, b))

print len(c)

t2 = time.time()

print round(t2 - t1, 5), "s"
