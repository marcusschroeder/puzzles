'''
https://projecteuler.net/problem=62

The cube, 41063625 (3453), can be permuted to produce two other cubes: 56623104 (3843) and 66430125 (4053). In fact, 41063625 is the smallest cube which has exactly three permutations of its digits which are also cube.

Find the smallest cube for which exactly five permutations of its digits are cube.
'''
import time

t1 = time.time()
bucket = {}
for i in range(1, 1000000):
    cube = i*i*i
    a = ''.join(sorted(str(cube)))

    if a not in bucket:
        bucket[a] = [i]
    else:
        bucket[a].append(i)

    if len(bucket[a]) > 4:
        print a, bucket[a], bucket[a][0]*bucket[a][0]*bucket[a][0]
        break

t2 = time.time()

print (t2 - t1) * 1000, "ms"