'''
https://projecteuler.net/problem=60

The primes 3, 7, 109, and 673, are quite remarkable. By taking any two primes and concatenating them in any order the result will always be prime. For example, taking 7 and 109, both 7109 and 1097 are prime. The sum of these four primes, 792, represents the lowest sum for a set of four primes with this property.

Find the lowest sum for a set of five primes for which any two primes concatenate to produce another prime.

############
results:
1229 primes found
5-tuple: 1
min_set: [13, 5197, 5701, 6733, 8389] 26033
None
time taken: 131.76 s
comparisons:  12882521
'''
import tools
import time


def is_prime_pair_set(l):
    global comparisons
    is_prime_pairs_set = True

    # creating a list of tuples containing the last (newest) entry and one old entry
    k = [(j, l[-1]) for j in l[:-1]]

    for m in k:
        comparisons += 1

        if not (primes_tables[m[1]][m[0]] and primes_tables[m[0]][m[1]]):
            is_prime_pairs_set = False
            break

    return is_prime_pairs_set


def is_valid_tuple(t):
    return t[-1] > t[-2] and is_prime_pair_set(t)


def get_new_tuples(old_tuple):
    t = filter(lambda m: is_valid_tuple(old_tuple + [m]), [k for k in primes if k not in old_tuple])
    return map(lambda m: old_tuple + [m], t)


def find_prime_pair_sets(tuples):
    x = []
    map(lambda v: x.extend(get_new_tuples(v)), tuples)
    return x


def find_min_set(s):
    min_set = []
    min_value = 100000000
    for i in s:
        if sum(i) < min_value:
            min_set = i
            min_value = sum(i)

    print min_set, sum(min_set)


comparisons = 0
limit = 10000
t1 = time.time()
primes = tools.primes(limit)
print len(primes), "primes found"
primes.remove(2)
primes.remove(5)

# to look up prime pairs
primes_tables = []
for i in range(limit):
    if i in primes:
        primes_tables.append([False] * limit)
    else:
        primes_tables.append(False)

for i, v_i in enumerate(primes):
    for j, v_j in enumerate(primes[i + 1:]):
        primes_tables[v_i][v_j] = tools.is_prime(int(str(v_i) + str(v_j)))
        primes_tables[v_j][v_i] = tools.is_prime(int(str(v_j) + str(v_i)))

y = [[i] for i in primes]

for i in range(2, 6):
    y = find_prime_pair_sets(y)

print str(i) + "-tuple:", len(y)
print "min_set:", find_min_set(y)

t2 = time.time()

print "time taken:", round(t2 - t1, 3), "s"
print "comparisons: ", comparisons