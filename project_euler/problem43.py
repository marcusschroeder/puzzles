'''
https://projecteuler.net/problem=43
'''

import itertools
import tools
import time


t1 = time.time()
t = list(itertools.permutations('1234567890', 10))
p = tools.primes(17)

# for each permutation i = (1,4,0,6,3,5,7,2,8,9) create string and check that substrings D_2 to D_8
#  are divisible by prime number in p
total = filter(lambda x: all(int(''.join(x)[j + 1: j + 4]) % v_j == 0 for j, v_j in enumerate(p)), t)

print "permutations", len(t)
print "sum", sum(int(''.join(i)) for i in total)

t2 = time.time()
print round(t2 - t1, 2), "s"